# Config

---
## Presentation

This project gathers all my relevant configuration files, I made them public for anyone interested.

---
## Installation

```console
$ git clone git@gitlab.com:stephane.tzvetkov/config.git
$ cd config
```

---
## Usage

The master branch does not contain any configuration files. I have a branch per computer I own (or
have owned), so you will first have to switch to the branch of the computer you are interested in,
e.g. my [Artix](https://artixlinux.org/) laptop:
```console
$ git switch artix-laptop
```

Now you will see that a part of my file hierarchy structure is replicated in the selected branch,
in order to facilitate navigation. For example, my user config files are located in
`./home/stephane/.config/`.

Let's say you are interested in my `$HOME/.xinitrc` config file, you will backup or remove your own
`~/.xinitrc` config file, and just copy or symlink mine. E.g.
```console
$ mkdir -p $HOME/.backup
$ mv $HOME/.xinitrc $HOME/.backup/xinitrc
$ ln -s ./home/stephane/.xinitrc $HOME/.xinitrc # or `$ cp ./hom/stepane/.xinitrc $HOME/.xinitrc`
```

---
## Configuration file management

I manage my configuration files in this repository thanks to the [bare-git
technique](https://www.atlassian.com/git/tutorials/dotfiles), that I "adapted" in [one of my
cheatsheets](https://stephane-cheatsheets.readthedocs.io/en/latest/versioning/bare-git/). This is a
pretty simple method that I recommand, but there are [many other way of doing
it](https://github.com/twpayne/chezmoi/blob/master/docs/COMPARISON.md) you might be interested in.

---
## Packages

You can also check the packages I have explicitly installed on my system in the
`<computer>-packages` file next to this README (e.g.
[artix-laptop-packages](./artix-laptop-packages)).

